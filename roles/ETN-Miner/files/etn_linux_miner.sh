#!/bin/bash
apt update && apt-get install automake autoconf pkg-config libssl-dev libjansson-dev libgmp-dev libcurl4-openssl-dev make g++ -y
apt-get install git -y
git clone https://github.com/tpruvot/cpuminer-multi etn_cpu_linux_miner
cd etn_cpu_linux_miner
./build.sh